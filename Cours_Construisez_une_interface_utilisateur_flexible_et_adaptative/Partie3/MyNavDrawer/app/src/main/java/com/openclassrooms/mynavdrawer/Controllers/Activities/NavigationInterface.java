package com.openclassrooms.mynavdrawer.Controllers.Activities;

public interface NavigationInterface {
    void updateFavoriteCounter(int counter);
    String getFavoriteCounter();
}
